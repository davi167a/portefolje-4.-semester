﻿using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Linq;
using System.Numerics;
using TitanicMLsolutionML.Model;

namespace TitanicMLsolution
{
    class Program
    {
        private static string TEST_DATA_FILEPATH = @"C:\Users\David\Desktop\Semester 4\portefolje\Machine Learning\Titanic\test.csv";
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //MLContext mlContext = new MLContext();
            //IDataView testingDataView = mlContext.Data.LoadFromTextFile<ModelInput>(
            //                    path: TEST_DATA_FILEPATH,
            //                    hasHeader: true,
            //                    separatorChar: ',',
            //                    allowQuoting: true,
            //                    allowSparse: false,);

            //var rows = testingDataView.Preview(20);

            //string modelPath = @"C:\Users\David\AppData\Local\Temp\MLVSTools\TitanicMLsolutionML\TitanicMLsolutionML.Model\MLModel.zip";
            //DataViewSchema modelInputSchema;
            //ITransformer mlModel = mlContext.Model.Load(modelPath, out modelInputSchema);

            ////Predicted data
            //IDataView predictions = mlModel.Transform(testingDataView);

            ////var values = mlContext.Data.CreateEnumerable<TransformedData>(
            ////transformedData, reuseRowObject: false);

            //var scoreColumn = predictions.GetColumn<UInt32>("Survived");
            //var scoreArray = scoreColumn.ToArray();

            //Add input data
            //var input = new ModelInput();
            //input.PassengerId = 892;
            //input.Pclass = 3;
            //input.Name = "Kelly, Mr. James";
            //input.Sex = "male";
            //input.Age = 34.5f;
            //input.SibSp = 0;
            //input.Parch = 0;
            //input.Ticket = "330911";
            //input.Fare = 7.8292f;
            //input.Cabin = "";
            //input.Embarked = "Q";


            var input = new ModelInput();
            input.PassengerId = 892;

            Console.WriteLine("Hvad er dit navn? [efternavn, Mr./Miss./Mrs. Navn Mellemnavn(e)]");
            input.Name = Console.ReadLine();

            Console.WriteLine("Hvor gammel er du?*");
            input.Age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hvilket køn har du? [male/female]*");
            input.Sex = Console.ReadLine();

            Console.WriteLine("Hvor rig er du? [1: under gnmsn. 2: gnmsn. 3: over gnmsn.]");
            Console.WriteLine("Hvor god er du til at bruge penge? [1: nærrig 2: normal 3: BIG spender!]");
            Console.WriteLine("Hvad er din score lagt sammen? [eks. 2 + 1 = 3]*");
            int score = Convert.ToInt32(Console.ReadLine());
            switch (score)
            {
                case 2:
                    input.Fare = 8;
                    input.Pclass = 3;
                    input.Cabin = "";
                    input.Ticket = "330911";
                    break;
                case 3:
                    input.Fare = 29.125f;
                    input.Pclass = 3;
                    input.Cabin = "";
                    input.Ticket = "382652";
                    break;
                case 4:
                    input.Fare = 96.875f;
                    input.Pclass = 2;
                    input.Cabin = "";
                    input.Ticket = "240276";
                    break;
                case 5:
                    input.Fare = 285.375f;
                    input.Pclass = 1;
                    input.Cabin = "C53";
                    input.Ticket = "2543";
                    break;
                case 6:
                    input.Fare = 2217.792f;
                    input.Pclass = 1;
                    input.Cabin = "C55 C57";
                    input.Ticket = "PC 17483";
                    break;
                default:
                    Console.WriteLine("Noget gik galt!");
                    break;
            }

            Console.WriteLine("Til følgende spørgsmål skal du forestille dig, at du skal på et cruise-skib. Svar ud fra det scenarie du har sat op:");

            Console.WriteLine("Hvor mange af din(e) (sted-)søskende samt ægtefælle ville komme med?*");
            input.SibSp = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hvor mange af din(e) børn samt forældre ville komme med?*");
            input.Parch = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hvor ville du være steget på, hvis du kunne vælge mellem følgende:");
            Console.WriteLine("(Svar med et bogstav)[C = Cherbourg, Q = Queenstown, S = Southampton]*");
            input.Embarked = Console.ReadLine();

            Console.WriteLine("\n\n");

            // Load model and predict output of sample data
            ModelOutput result = ConsumeModel.Predict(input);

            if (result.Prediction == "0")
            {
                Console.WriteLine("Computeren mener, at du ville dø");
            }
            else
            {
                Console.WriteLine("Computeren mener, at du ville overleve");
            }
            Console.WriteLine("Din score blev: " + result.Score[1] + "(jo tættere på 1, desto større chance for at overleve)");
        }
    }
}
