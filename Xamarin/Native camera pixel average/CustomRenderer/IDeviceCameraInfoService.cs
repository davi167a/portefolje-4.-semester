﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CustomRenderer
{
    public interface IDeviceCameraInfoService
    {
        Color GetAverageCameraPixelColor();
        event EventHandler AverageColorUpdated;
    }
}
