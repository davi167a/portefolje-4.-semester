﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Xamarin.Forms;

namespace CustomRenderer
{
    public class CameraViewModel : ViewModelBase
    {
        private System.Drawing.Color _currentColor;

        IDeviceCameraInfoService cameraInfo;

        public System.Drawing.Color CurrentColor
        {
            get { return _currentColor; }
            set { _currentColor = value; OnPropertyChanged(nameof(CurrentColor)); }
        }

        public CameraViewModel()
        {
            UpdateColor_Cmd = new Command(UpdateColor);
            cameraInfo = DependencyService.Get<IDeviceCameraInfoService>();
            cameraInfo.AverageColorUpdated += CameraInfo_AverageColorUpdated;
            MessagingCenter.Subscribe<List<System.Drawing.Color>>(this, "ColorMessage", (args) =>
            {
                CurrentColor = args[0];
            });
        }

        private void CameraInfo_AverageColorUpdated(object sender, EventArgs e)
        {
            CurrentColor = cameraInfo.GetAverageCameraPixelColor();
        }

        private void UpdateColor()
        {
            CurrentColor = cameraInfo.GetAverageCameraPixelColor();
        }
        public Command UpdateColor_Cmd { get; }

    }
}
