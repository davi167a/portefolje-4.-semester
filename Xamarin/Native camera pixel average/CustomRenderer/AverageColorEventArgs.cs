﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CustomRenderer
{
    public class AverageColorEventArgs : EventArgs
    {
        public Color Color { get; set; }
        public AverageColorEventArgs(Color color)
        {
            Color = color;
        }
    }
}
