﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using CustomRenderer;
using CustomRenderer.Droid;
using Android.App;
using Android.Content;
using Android.Hardware;
using Android.Views;
using Android.Graphics;
using Android.Widget;
using System.Threading.Tasks;
using System.Collections.Generic;

[assembly: ExportRenderer(typeof(CameraPage), typeof(CameraPageRenderer))]
[assembly: Dependency(typeof(CustomRenderer.Droid.CameraPageRenderer))]
namespace CustomRenderer.Droid
{
    public class CameraPageRenderer : PageRenderer, IDeviceCameraInfoService, TextureView.ISurfaceTextureListener
    {
        global::Android.Hardware.Camera camera;
        //global::Android.Widget.Button takePhotoButton;
        //global::Android.Widget.Button toggleFlashButton;
        //global::Android.Widget.Button switchCameraButton;
        global::Android.Views.View view;

        public System.Drawing.Color AveragePixelColor { get => averagePixelColor;
            set { averagePixelColor = value; OnAverageColorChanged(value); }
        }
        public event EventHandler AverageColorUpdated;

        Activity activity;
        CameraFacing cameraType;
        TextureView textureView;
        SurfaceTexture surfaceTexture;

        bool flashOn;

        public CameraPageRenderer()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                SetupUserInterface();
                SetupEventHandlers();
                AddView(view);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
            }
        }

        void SetupUserInterface()
        {
            activity = this.Context as Activity;
            view = activity.LayoutInflater.Inflate(Resource.Layout.CameraLayout, this, false);
            cameraType = CameraFacing.Back;

            textureView = view.FindViewById<TextureView>(Resource.Id.textureView);
            textureView.SurfaceTextureListener = this;
        }

        void SetupEventHandlers()
        {
            //takePhotoButton = view.FindViewById<global::Android.Widget.Button>(Resource.Id.takePhotoButton);
            //takePhotoButton.Click += TakePhotoButtonTapped;

            //switchCameraButton = view.FindViewById<global::Android.Widget.Button>(Resource.Id.switchCameraButton);
            //switchCameraButton.Click += SwitchCameraButtonTapped;

            //toggleFlashButton = view.FindViewById<global::Android.Widget.Button>(Resource.Id.toggleFlashButton);
            //toggleFlashButton.Click += ToggleFlashButtonTapped;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            var msw = MeasureSpec.MakeMeasureSpec(r - l, MeasureSpecMode.Exactly);
            var msh = MeasureSpec.MakeMeasureSpec(b - t, MeasureSpecMode.Exactly);

            view.Measure(msw, msh);
            view.Layout(0, 0, r - l, b - t);
        }

        int counter = 0;
        private static System.Drawing.Color averagePixelColor;

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {
            if (counter == 5)
            {
                Bitmap bitmap = textureView.Bitmap;

                Task.Factory.StartNew(() =>
                    AveragePixelColor = GetAverageColorFromBitmap(bitmap));

                counter = 0;
            }
            counter++;
        }

        private System.Drawing.Color GetAverageColorFromBitmap(Bitmap bitmap)
        {

            //Used for tally
            int r = 0;
            int g = 0;
            int b = 0;

            int total = 0;

            bitmap = Bitmap.CreateScaledBitmap(bitmap, 16, 9, false);

            if (bitmap == null)
                return new System.Drawing.Color();

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Android.Graphics.Color clr = new Android.Graphics.Color(bitmap.GetColor(x, y).ToArgb());

                    r += clr.R;
                    g += clr.G;
                    b += clr.B;

                    total++;
                }
            }

            //Calculate average
            r = r / total;
            g = g / total;
            b = b / total;

            return System.Drawing.Color.FromArgb(r, g, b);
        }

        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
        {
            camera = global::Android.Hardware.Camera.Open((int)cameraType);
            textureView.LayoutParameters = new FrameLayout.LayoutParams(width, height);
            surfaceTexture = surface;

            camera.SetPreviewTexture(surface);
            PrepareAndStartCamera();
        }

        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            camera.StopPreview();
            camera.Release();
            return true;
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
        {
            PrepareAndStartCamera();
        }

        void PrepareAndStartCamera()
        {
            camera.StopPreview();

            var display = activity.WindowManager.DefaultDisplay;
            if (display.Rotation == SurfaceOrientation.Rotation0)
            {
                camera.SetDisplayOrientation(90);
            }

            if (display.Rotation == SurfaceOrientation.Rotation270)
            {
                camera.SetDisplayOrientation(180);
            }

            camera.StartPreview();
        }

        //void ToggleFlashButtonTapped(object sender, EventArgs e)
        //{
        //    flashOn = !flashOn;
        //    if (flashOn)
        //    {
        //        if (cameraType == CameraFacing.Back)
        //        {
        //            toggleFlashButton.SetBackgroundResource(Resource.Drawable.FlashButton);
        //            cameraType = CameraFacing.Back;

        //            camera.StopPreview();
        //            camera.Release();
        //            camera = global::Android.Hardware.Camera.Open((int)cameraType);
        //            var parameters = camera.GetParameters();
        //            parameters.FlashMode = global::Android.Hardware.Camera.Parameters.FlashModeTorch;
        //            camera.SetParameters(parameters);
        //            camera.SetPreviewTexture(surfaceTexture);
        //            PrepareAndStartCamera();
        //        }
        //    }
        //    else
        //    {
        //        toggleFlashButton.SetBackgroundResource(Resource.Drawable.NoFlashButton);
        //        camera.StopPreview();
        //        camera.Release();

        //        camera = global::Android.Hardware.Camera.Open((int)cameraType);
        //        var parameters = camera.GetParameters();
        //        parameters.FlashMode = global::Android.Hardware.Camera.Parameters.FlashModeOff;
        //        camera.SetParameters(parameters);
        //        camera.SetPreviewTexture(surfaceTexture);
        //        PrepareAndStartCamera();
        //    }
        //}

        //void SwitchCameraButtonTapped(object sender, EventArgs e)
        //{
        //    if (cameraType == CameraFacing.Front)
        //    {
        //        cameraType = CameraFacing.Back;

        //        camera.StopPreview();
        //        camera.Release();
        //        camera = global::Android.Hardware.Camera.Open((int)cameraType);
        //        camera.SetPreviewTexture(surfaceTexture);
        //        PrepareAndStartCamera();
        //    }
        //    else
        //    {
        //        cameraType = CameraFacing.Front;

        //        camera.StopPreview();
        //        camera.Release();
        //        camera = global::Android.Hardware.Camera.Open((int)cameraType);
        //        camera.SetPreviewTexture(surfaceTexture);
        //        PrepareAndStartCamera();
        //    }
        //}

        //async void TakePhotoButtonTapped(object sender, EventArgs e)
        //{
        //    camera.StopPreview();

        //    var image = textureView.Bitmap;

        //    try
        //    {
        //        var absolutePath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim).AbsolutePath;
        //        var folderPath = absolutePath + "/Camera";
        //        var filePath = System.IO.Path.Combine(folderPath, string.Format("photo_{0}.jpg", Guid.NewGuid()));

        //        var fileStream = new FileStream(filePath, FileMode.Create);
        //        await image.CompressAsync(Bitmap.CompressFormat.Jpeg, 50, fileStream);
        //        fileStream.Close();
        //        image.Recycle();

        //        var intent = new Android.Content.Intent(Android.Content.Intent.ActionMediaScannerScanFile);
        //        var file = new Java.IO.File(filePath);
        //        var uri = Android.Net.Uri.FromFile(file);
        //        intent.SetData(uri);
        //        MainActivity.Instance.SendBroadcast(intent);
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(@"				", ex.Message);
        //    }

        //    camera.StartPreview();
        //}

        public System.Drawing.Color GetAverageCameraPixelColor()
        {
            return AveragePixelColor;
        }

        public void OnAverageColorChanged(System.Drawing.Color color)
        {
            var colors = new List<System.Drawing.Color>() { color };
            MessagingCenter.Send<List<System.Drawing.Color>>(colors, "ColorMessage");
            AverageColorUpdated?.Invoke(this, new AverageColorEventArgs(color));
        }
    }
}

