﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddWorkout : ContentPage
    {
        public AddWorkout()
        {
            InitializeComponent();
        }

        private async void Save_Btn_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as ViewModels.AddWorkoutVM;
            using (WorkoutContext context = new WorkoutContext())
            {
                context.Workouts.Add(new Models.Workout()
                {
                    Name = vm.Name,
                    WorkoutPlanId = 1
                });

                await context.SaveChangesAsync();
            }

            await Navigation.PopModalAsync();
        }
    }
}