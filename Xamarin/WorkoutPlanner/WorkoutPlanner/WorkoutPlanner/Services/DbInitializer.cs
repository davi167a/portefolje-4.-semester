﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkoutPlanner.Models;

namespace WorkoutPlanner.Services
{
    public static class DbInitializer
    {
        public static void Initialize(WorkoutContext context)
        {
            

            // Look for any students.
            if (context.Exercises.Any())
            {
                return;   // DB has been seeded
            }

            var exercises = new Exercise[]
            {
                new Exercise(){
                    Name = "Push-ups",
                    Description = "The wider your hands are, the more focus you put on your chest. The closer your hands, the more you focus on your biceps",
                    IsCustom = false,
                    MaxReps = 40,
                    MaxTimeInSec = null,
                    MaxDistanceInMeters = null,
                    Image = null
                }
            };
            context.Exercises.AddRange(exercises);
            context.SaveChanges();

            var exerciseMeasureTypes = new ExerciseMeasureType[]
            {
                new ExerciseMeasureType(){ MeasureType = ExerciseMeasurement.Repetitions },
                new ExerciseMeasureType(){ MeasureType = ExerciseMeasurement.Time },
                new ExerciseMeasureType(){ MeasureType = ExerciseMeasurement.Distance }
            };
            context.ExerciseMeasureTypes.AddRange(exerciseMeasureTypes);
            context.SaveChanges();

            var exerciseMeasureTypeMaps = new ExerciseMeasureTypeMap[]
            {
                new ExerciseMeasureTypeMap(){ ExerciseId = 1, ExerciseMeasureTypeId = 1 }
            };
            context.ExerciseMeasureTypeMaps.AddRange(exerciseMeasureTypeMaps);
            context.SaveChanges();

            var muscleTarget = new MuscleTarget[]
            {
                new MuscleTarget(){ Group = MuscleGroups.Biceps },
                new MuscleTarget(){ Group = MuscleGroups.Calfs },
                new MuscleTarget(){ Group = MuscleGroups.Chest },
                new MuscleTarget(){ Group = MuscleGroups.Glutes },
                new MuscleTarget(){ Group = MuscleGroups.LowerAbs },
                new MuscleTarget(){ Group = MuscleGroups.LowerBag },
                new MuscleTarget(){ Group = MuscleGroups.Shoulders },
                new MuscleTarget(){ Group = MuscleGroups.Thighs },
                new MuscleTarget(){ Group = MuscleGroups.Triceps },
                new MuscleTarget(){ Group = MuscleGroups.UpperAbs },
                new MuscleTarget(){ Group = MuscleGroups.UpperBag }
            };
            context.MuscleTargets.AddRange(muscleTarget);
            context.SaveChanges();

            var exerciseMuscleMaps = new ExerciseMuscleMap[]
{
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 1 },
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 3 },
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 6 },
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 7 },
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 9 },
                new ExerciseMuscleMap(){ ExerciseId = 1, MuscleTargetId = 11 }
};
            context.ExerciseMuscleMaps.AddRange(exerciseMuscleMaps);
            context.SaveChanges();

            var workoutPlans = new WorkoutPlan[]
{
                new WorkoutPlan(){
                    Name = "Perfect balance",
                    Plan = WorkoutDayPlan.TwoDaysOnOneDayOffWOWeekend
                }
};
            context.WorkoutPlans.AddRange(workoutPlans);
            context.SaveChanges();

            var workouts = new Workout[]
{
                new Workout(){
                    WorkoutPlanId = 1,
                    Name = "Only Pushups"
                }
};
            context.Workouts.AddRange(workouts);
            context.SaveChanges();

            var exerciseInstances = new ExerciseInstance[]
{
                new ExerciseInstance(){
                    ExerciseId = 1,
                    WorkoutId = 1,
                    Reps = 20
                }
};
            context.ExerciseInstances.AddRange(exerciseInstances);
            context.SaveChanges();

        }
    }
}
