﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WorkoutPlanner.Models;
using Xamarin.Essentials;

namespace WorkoutPlanner.Services
{
    public class WorkoutContext : DbContext
    {
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseInstance> ExerciseInstances { get; set; }
        public DbSet<ExerciseMeasureType> ExerciseMeasureTypes { get; set; }
        public DbSet<MuscleTarget> MuscleTargets { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<WorkoutPlan> WorkoutPlans { get; set; }
        public DbSet<ExerciseMeasureTypeMap> ExerciseMeasureTypeMaps { get; set; }
        public DbSet<ExerciseMuscleMap> ExerciseMuscleMaps { get; set; }

        public WorkoutContext()
        {
            SQLitePCL.Batteries_V2.Init();
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>().ToTable("Exercise");
            modelBuilder.Entity<ExerciseInstance>().ToTable("ExerciseInstance");
            modelBuilder.Entity<ExerciseMeasureType>().ToTable("ExerciseMeasureType");
            modelBuilder.Entity<MuscleTarget>().ToTable("MuscleTarget");
            modelBuilder.Entity<Workout>().ToTable("Workout");
            modelBuilder.Entity<WorkoutPlan>().ToTable("WorkoutPlan");
            modelBuilder.Entity<ExerciseMeasureTypeMap>().ToTable("ExerciseMeasureTypeMap");
            modelBuilder.Entity<ExerciseMuscleMap>().ToTable("ExerciseMuscleMap");

            //creating composite key by combining UserId and SurfSpotId as the PK for FavoriteSpotMap.
            modelBuilder.Entity<ExerciseMeasureTypeMap>().HasKey(e => new { e.ExerciseId, e.ExerciseMeasureTypeId });
            modelBuilder.Entity<ExerciseMuscleMap>().HasKey(e => new { e.ExerciseId, e.MuscleTargetId });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string dbPath = Path.Combine(FileSystem.AppDataDirectory, "workouts.db3");

            optionsBuilder
                .UseSqlite($"Filename={dbPath}");
        }
    }
}
