﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.Models;
using WorkoutPlanner.Services;

namespace WorkoutPlanner.Repositories
{
    public class WorkoutRepo
    {
        public async Task<List<Workout>> GetAll()
        {
            using (WorkoutContext context = new WorkoutContext())
            {
                return await context.Workouts.ToListAsync();
            }
        }
    }
}
