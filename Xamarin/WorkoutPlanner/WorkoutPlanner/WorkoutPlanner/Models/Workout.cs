﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public class Workout
    {
        public Workout()
        {
            //this.ExerciseInstances = new HashSet<ExerciseInstance>();
        }

        public long Id { get; set; }
        public long WorkoutPlanId { get; set; }
        public WorkoutPlan WorkoutPlan { get; set; }

        public string Name { get; set; }
        
        public virtual IEnumerable<ExerciseInstance> ExerciseInstances { get; set; }

    }
}
