﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public class ExerciseInstance
    {
        public long Id { get; set; }
        public long ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        public long WorkoutId { get; set; }
        public Workout Workout { get; set; }
        public int? Reps { get; set; }
        public int? TimeInSec { get; set; }
        public int? DistanceInMeters { get; set; }

    }
}
