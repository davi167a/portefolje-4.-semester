﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public class ExerciseMuscleMap
    {
        public long ExerciseId { get; set; }
        public long MuscleTargetId { get; set; }

        public Exercise Exercise { get; set; }
        public virtual MuscleTarget MuscleTarget { get; set; }
    }
}
