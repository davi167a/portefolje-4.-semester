﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using WorkoutPlanner.Models;
using WorkoutPlanner.Repositories;
using WorkoutPlanner.Services;

namespace WorkoutPlanner.ViewModels
{
    public class WorkoutsVM : ViewModelBase
    {
        private List<Workout> _workouts;

        public List<Workout> Workouts
        {
            get
            {
                return _workouts;
            }
            set
            {
                _workouts = value;
                OnPropertyChanged("Workouts");
            }
        }

        public void UpdateWorkouts()
        {
            using (WorkoutContext context = new WorkoutContext())
            {
                Workouts = context.Workouts.ToList();
            }
        }

    }
}
