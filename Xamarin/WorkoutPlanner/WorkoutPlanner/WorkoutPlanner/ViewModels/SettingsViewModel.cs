﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace WorkoutPlanner.ViewModels
{
    class SettingsViewModel : ViewModelBase
    {
        public string PhoneNumber { get; set; } = "53335557";

        public Command<Label> OpenPhoneDialer { get; }

        public SettingsViewModel()
        {
            OpenPhoneDialer = new Command<Label>((Label label) => 
                Launcher.OpenAsync($"tel:{label.Text}"));
        }


    }
}
