﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp1
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //Make MainPage be a NavigationPage
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
