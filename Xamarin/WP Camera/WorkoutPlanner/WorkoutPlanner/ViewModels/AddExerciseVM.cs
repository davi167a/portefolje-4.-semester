﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkoutPlanner.Models;
using WorkoutPlanner.Services;
using Xamarin.Forms;

namespace WorkoutPlanner.ViewModels
{
    class AddExerciseVM : ViewModelBase
    {
        CameraUtility camera;

        private ImageSource _exerciseImage;

        public ImageSource ExerciseImage
        {
            get { return _exerciseImage; }
            set { _exerciseImage = value; OnPropertyChanged(nameof(ExerciseImage)); }
        }

        public string ImagePath { get; set; }

        private string name;
        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }

        public AddExerciseVM()
        {
            camera = new CameraUtility();

            TakePic_Cmd = new Command(TakePic);
        }

        private async void TakePic()
        {
            ImagePath = await camera.TakePhoto();
            ExerciseImage = ImageSource.FromFile(ImagePath);
        }
        public Command TakePic_Cmd { get; }
    }
}
