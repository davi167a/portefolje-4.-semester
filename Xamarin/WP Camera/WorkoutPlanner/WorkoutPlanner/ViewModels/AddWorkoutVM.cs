﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkoutPlanner.Models;
using WorkoutPlanner.Services;

namespace WorkoutPlanner.ViewModels
{
    public class AddWorkoutVM : ViewModelBase
    {
        private string name;

        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }
        public List<WorkoutDayPlan> WorkoutDayPlans { get; set; } = new List<WorkoutDayPlan>();

        public AddWorkoutVM()
        {
            WorkoutDayPlans.Add(WorkoutDayPlan.OneDayOnOneDayOff);
            WorkoutDayPlans.Add(WorkoutDayPlan.OneDayOnOneDayOffWOWeekend);
            WorkoutDayPlans.Add(WorkoutDayPlan.TwoDaysOnOneDayOff);
            WorkoutDayPlans.Add(WorkoutDayPlan.TwoDaysOnOneDayOffWOWeekend);
            WorkoutDayPlans.Add(WorkoutDayPlan.ThreeDaysOnOneDayOff);
            WorkoutDayPlans.Add(WorkoutDayPlan.WeekDays);
            WorkoutDayPlans.Add(WorkoutDayPlan.EveryDay);
            WorkoutDayPlans.Add(WorkoutDayPlan.CustomWeekBased);
        }
    }
}
