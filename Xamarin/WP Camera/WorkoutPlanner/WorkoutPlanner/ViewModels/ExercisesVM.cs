﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkoutPlanner.Models;
using WorkoutPlanner.Services;

namespace WorkoutPlanner.ViewModels
{
    public class ExercisesVM : ViewModelBase
    {
        private List<Exercise> _exercises;

        public List<Exercise> Exercises
        {
            get
            {
                return _exercises;
            }
            set
            {
                _exercises = value;
                OnPropertyChanged("Exercises");
            }
        }

        public void UpdateWorkouts()
        {
            using (WorkoutContext context = new WorkoutContext())
            {
                Exercises = context.Exercises.ToList();
            }
        }
    }
}
