﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Progress : CarouselPage
    {
        public Progress()
        {
            InitializeComponent();
        }
    }
}