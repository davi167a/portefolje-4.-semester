﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Exercises : ContentPage
    {
        ExercisesVM vm;
        public Exercises()
        {
            InitializeComponent();
            vm = BindingContext as ExercisesVM;
            Application.Current.PageAppearing += Current_PageAppearing;

        }

        private void Current_PageAppearing(object sender, Page e)
        {
            if (e == this || e is NavigationPage)
            {
                vm.UpdateWorkouts();
            }
        }

        private async void AddExercise_Btn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddExercise());
        }
    }
}