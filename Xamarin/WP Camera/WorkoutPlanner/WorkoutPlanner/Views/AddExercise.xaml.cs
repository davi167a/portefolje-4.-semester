﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddExercise : ContentPage
    {
        public AddExercise()
        {
            InitializeComponent();
        }

        private async void Save_Btn_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as ViewModels.AddExerciseVM;
            using (WorkoutContext context = new WorkoutContext())
            {
                context.Exercises.Add(new Models.Exercise()
                {
                    Name = vm.Name,
                    Image = vm.ImagePath
                });

                await context.SaveChangesAsync();
            }

            await Navigation.PopModalAsync();
        }
    }
}