﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.Repositories;
using WorkoutPlanner.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Workouts : ContentPage
    {
        WorkoutsVM vm;
        public Workouts()
        {
            InitializeComponent();
            vm = BindingContext as WorkoutsVM;
            Application.Current.PageAppearing += Current_PageAppearing;
        }

        private void Current_PageAppearing(object sender, Page e)
        {
            if (e == this)
            {
                vm.UpdateWorkouts();
            }
        }

        private async void AddWorkout_Btn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddWorkout());
        }
    }
}