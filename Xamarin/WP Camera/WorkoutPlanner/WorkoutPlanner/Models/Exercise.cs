﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace WorkoutPlanner.Models
{
    public class Exercise
    {
        public Exercise()
        {
            //this.ExerciseInstances = new HashSet<ExerciseInstance>();
            //this.MuscleGroups = new HashSet<MuscleTarget>();
            //this.MeasureTypes = new HashSet<ExerciseMeasureType>();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsCustom { get; set; }
        public int? MaxReps { get; set; }
        public int? MaxTimeInSec { get; set; }
        public int? MaxDistanceInMeters { get; set; }
        public string Image { get; set; }
       
        public virtual IEnumerable<ExerciseInstance> ExerciseInstances { get; set; }
        public virtual IEnumerable<MuscleTarget> MuscleGroups { get; set; }
        public virtual IEnumerable<ExerciseMeasureType> MeasureTypes { get; set; }


    }
}
