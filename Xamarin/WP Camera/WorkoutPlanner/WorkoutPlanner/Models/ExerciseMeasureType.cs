﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public enum ExerciseMeasurement
    {
        Repetitions,
        Time,
        Distance
    }
    public class ExerciseMeasureType
    {
        public ExerciseMeasureType()
        {
            //this.Exercises = new HashSet<Exercise>();
        }

        public long Id { get; set; }
        public ExerciseMeasurement MeasureType { get; set; }

        public virtual IEnumerable<Exercise> Exercises { get; set; }
    }
}
