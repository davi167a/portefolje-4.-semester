﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public enum WorkoutDayPlan
    {
        OneDayOnOneDayOffWOWeekend,
        OneDayOnOneDayOff,
        TwoDaysOnOneDayOffWOWeekend,
        TwoDaysOnOneDayOff,
        ThreeDaysOnOneDayOff,
        WeekDays,
        EveryDay,
        CustomWeekBased
    }

    public class WorkoutPlan
    {


        public WorkoutPlan()
        {
            //this.Workouts = new HashSet<Workout>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public WorkoutDayPlan Plan { get; set; }
        public bool IsPlanOnOffBased { get => Plan != WorkoutDayPlan.CustomWeekBased; }

        //For on/off based plans
        public int? CurrentOnCount { get; set; }
        public int? MaxOnCount { get; set; }
        public bool? IsWeekendCounted { get; set; }

        //For week-based plans
        public bool? IsMondayOn { get; set; }
        public bool? IsTuesdayOn { get; set; }
        public bool? IsWednesdayOn { get; set; }
        public bool? IsThursdayOn { get; set; }
        public bool? IsFridayOn { get; set; }
        public bool? IsSaturdayOn { get; set; }
        public bool? IsSundayOn { get; set; }

        public virtual IEnumerable<Workout> Workouts { get; set; }

    }
}
