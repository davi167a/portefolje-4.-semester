﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public enum MuscleGroups
    {
        UpperAbs,
        LowerAbs,
        Biceps,
        Triceps,
        Chest,
        Shoulders,
        UpperBag,
        LowerBag,
        Glutes,
        Thighs,
        Calfs
    }

    public class MuscleTarget
    {


        public MuscleTarget()
        {
            //this.Exercises = new HashSet<Exercise>();
        }

        public long Id { get; set; }
        public MuscleGroups Group { get; set; }

        public virtual IEnumerable<Exercise> Exercises { get; set; }
    }
}
