﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkoutPlanner.Models
{
    public class ExerciseMeasureTypeMap
    {
        public long ExerciseId { get; set; }
        public long ExerciseMeasureTypeId { get; set; }

        public Exercise Exercise { get; set; }
        public virtual ExerciseMeasureType ExerciseMeasureType { get; set; }


    }
}
