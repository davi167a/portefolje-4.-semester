﻿using System;
using WorkoutPlanner.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkoutPlanner
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {

        }
    }
}
