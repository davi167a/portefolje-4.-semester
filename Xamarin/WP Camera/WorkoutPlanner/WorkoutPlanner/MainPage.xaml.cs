﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkoutPlanner.Services;
using Xamarin.Forms;

namespace WorkoutPlanner
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected async override void OnAppearing()
        {
            using (var context = new WorkoutContext())
            {
                DbInitializer.Initialize(context);
                //var workouts = context.Workouts.ToList();
                //long i = workouts[0].Id;
            }
        }
    }
}
